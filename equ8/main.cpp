#pragma once
#pragma comment(lib, "ZwSwapCert.lib")

#include <ntifs.h>
#include <ntddk.h>
#include <windef.h>
#include <intrin.h>
#include <stdarg.h>
#include <ntstrsafe.h>

#include "imports.hpp"
#include "shithook.hpp"
#include "utils.hpp"

extern "C" void ScDriverUnload(PDRIVER_OBJECT DriverObject);
extern "C" DRIVER_INITIALIZE DriverEntry;
extern "C" DRIVER_UNLOAD DriverUnload;

PVOID(__fastcall* MmGetSystemRoutineAddress_orig)(PUNICODE_STRING);
bool(__fastcall* IsProcessVerified_orig)(uintptr_t*, PEPROCESS);

LARGE_INTEGER systemtime;
TIME_FIELDS time;

PVOID RetAddr = NULL;

PVOID MmGetSystemRoutineAddress_hook(PUNICODE_STRING SystemRoutineName)
{
	LogToFile("[+] MmGetSystemRoutineAddress called %ws\n", SystemRoutineName->Buffer);
	return MmGetSystemRoutineAddress(SystemRoutineName);
}

bool IsProcessVerified_hook(uintptr_t* VerifiedProcessList, PEPROCESS proc)
{
	bool bypass = false;

	if (!RetAddr) 
		RetAddr = _ReturnAddress();

	if (RetAddr == _ReturnAddress())
	{
		static UNICODE_STRING booba = RTL_CONSTANT_STRING(L"booba");
		static UNICODE_STRING vs = RTL_CONSTANT_STRING(L"Community");

		PUNICODE_STRING imagename;
		SeLocateProcessImageName(proc, &imagename);
		if (RtlUnicodeStringContains(imagename, &booba, FALSE) || RtlUnicodeStringContains(imagename, &vs, FALSE))
		{
			//LogToFile("[+] Bypassing %wZ\n", imagename);
			bypass = true;
		}
		ExFreePool(imagename);
	}
	return (bypass ? true : IsProcessVerified_orig(VerifiedProcessList, proc));
}

void NotifyRoutine(PUNICODE_STRING FullImageName, HANDLE ProcessId, PIMAGE_INFO ImageInfo)
{
	if (!ProcessId && FullImageName && wcsstr(FullImageName->Buffer, L"EQU8_HELPER"))
	{
		KeQuerySystemTime(&systemtime);
		RtlTimeToTimeFields(&systemtime, &time);

		RetAddr = NULL;

		LogToFile("============== Log Date: %02d/%02d/%04d at %02d:%02d ==============\n", time.Day, time.Month, time.Year, time.Hour, time.Minute);
		LogToFile("[+] Found driver: %ws\n", FullImageName->Buffer);

		uintptr_t MmGetSystemRoutineAddress_IAT = reinterpret_cast<uintptr_t>(ImageInfo->ImageBase) + 0x2048;
		uintptr_t IsProcessVerified_address = reinterpret_cast<uintptr_t>(ImageInfo->ImageBase) + 0x15A4;

		auto to_write = &MmGetSystemRoutineAddress_hook;
		if (!SafeCopyMemory((PVOID)MmGetSystemRoutineAddress_IAT, &to_write, sizeof(uintptr_t)))
			LogToFile("[!] Failed to IAT hook MmGetSystemRoutineAddress\n");
		LogToFile("[+] Hooked MmGetSystemRoutineAddress\n");

		if (!TrampolineHook(&IsProcessVerified_hook, reinterpret_cast<PVOID>(IsProcessVerified_address), (PVOID*)&IsProcessVerified_orig))
			LogToFile("[!] Failed to setup trampoline hook for IsProcessVerified\n");
		LogToFile("[+] Hooked IsProcessVerified\n");
	}
}

void DriverUnload(DRIVER_OBJECT* DriverObject)
{
	PsRemoveLoadImageNotifyRoutine(&NotifyRoutine);
}

NTSTATUS TdDeviceClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

extern "C" NTSTATUS DriverEntry(_In_ PDRIVER_OBJECT  DriverObject, _In_ PUNICODE_STRING RegistryPath)
{
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = &TdDeviceClose;
	DriverObject->DriverUnload = &ScDriverUnload;

	PsSetLoadImageNotifyRoutine(&NotifyRoutine);

	return STATUS_SUCCESS;
}