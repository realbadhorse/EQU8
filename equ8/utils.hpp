#pragma once
#include <stdarg.h>
#include <ntstrsafe.h>
#include <ntifs.h>

__forceinline bool RtlUnicodeStringContains(PUNICODE_STRING Str, PUNICODE_STRING SubStr, BOOLEAN CaseInsensitive)
{
    if (Str == nullptr || SubStr == nullptr || Str->Length < SubStr->Length)
        return false;

    const USHORT numCharsDiff = (Str->Length - SubStr->Length) / sizeof(WCHAR);
    UNICODE_STRING slice = *Str;
    slice.Length = SubStr->Length;

    for (USHORT i = 0; i <= numCharsDiff; ++i, ++slice.Buffer, slice.MaximumLength -= sizeof(WCHAR))
    {
        if (RtlEqualUnicodeString(&slice, SubStr, CaseInsensitive))
            return true;
    }
    return false;
}

__forceinline NTSTATUS OpenFile(__in LPCWSTR FilePath, __in BOOLEAN Write, __in BOOLEAN Append, __out PHANDLE Handle)
{
    UNICODE_STRING      wzFilePath;
    OBJECT_ATTRIBUTES   objAttributes;
    IO_STATUS_BLOCK     ioStatus;
    RtlInitUnicodeString(&wzFilePath, FilePath);
    InitializeObjectAttributes(&objAttributes, &wzFilePath, OBJ_KERNEL_HANDLE, NULL, NULL);

    ACCESS_MASK mask = SYNCHRONIZE; 

    mask |= Write ? (Append ? FILE_APPEND_DATA : FILE_WRITE_DATA) : FILE_READ_DATA;

    return ZwCreateFile(Handle,
        mask,
        &objAttributes, &ioStatus,
        NULL, FILE_ATTRIBUTE_NORMAL,
        FILE_SHARE_READ, FILE_OPEN_IF,
        FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
}

NTSTATUS LogToFile(__in LPCSTR Format, __in_opt ...)
{
    IO_STATUS_BLOCK     ioStatus;
    HANDLE              hFile;
    CHAR                buffer[512];
    size_t              cb;
    NTSTATUS status = -1;
   
    //static LONG logger_lock = 0;
    //while(_InterlockedCompareExchange(&logger_lock, 1, 0)); //spin

    status = OpenFile(L"\\SystemRoot\\Temp\\log.log", TRUE, TRUE, &hFile);

    if (NT_SUCCESS(status))
    {
        RtlZeroMemory(buffer, 512);
        va_list va;
        va_start(va, Format);
        RtlStringCbVPrintfA(buffer, 512, Format, va);
        va_end(va);

        RtlStringCbLengthA(buffer, sizeof(buffer), &cb);
        ZwWriteFile(hFile, NULL, NULL, NULL, &ioStatus, buffer, (ULONG)cb, NULL, NULL);

        ZwFlushBuffersFile(hFile, &ioStatus);
        ZwClose(hFile);
       status = STATUS_SUCCESS;
    }
    //logger_lock = 0;
    return status;
}

__forceinline ULONG64 ResolveInstructionOffset(PCHAR instruction, ULONG64 offsetToEIP, ULONG64 offsetOfDataPtr)
{
    ULONG64 EIP = (ULONG64)instruction + offsetToEIP;	//instruction pointer to add offset to
    LONG32 offset = *(LONG32*)(instruction + offsetOfDataPtr);	//get the data ptr offset from the current EIP
    return EIP + offset;
}

//allows writing to read only memory
__forceinline bool SafeCopyMemory(void* address, void* buffer, size_t size)
{
    PMDL mdl = IoAllocateMdl(address, size, FALSE, FALSE, NULL);
    if (!mdl) { return false; }

    MmProbeAndLockPages(mdl, KernelMode, IoReadAccess);
    void* map = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmNonCached, NULL, FALSE, NormalPagePriority);
    MmProtectMdlSystemAddress(mdl, PAGE_READWRITE);

    memcpy(map, buffer, size);

    MmUnmapLockedPages(map, mdl);
    MmUnlockPages(mdl);
    IoFreeMdl(mdl);

    return true;
}