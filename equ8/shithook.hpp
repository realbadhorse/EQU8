#include <ntifs.h>
#include <ntddk.h>
#include <windef.h>
#include <intrin.h>

#include "instructiontable.hpp"

#define JMP_SIZE (14)

BYTE GetInstructionLength(BYTE table[], PBYTE instruction)
{
	BYTE i = table[*instruction++];
	return i < 0x10 ? i : GetInstructionLength(INSTRUCTION_TABLES[i - 0x10], instruction);
}

BOOL TrampolineHook(PVOID dest, PVOID src, PVOID* original)
{
	BOOL ret = FALSE;

	BYTE length = 0;
	for (PBYTE inst = (PBYTE)src; length < JMP_SIZE; )
	{
		BYTE l = GetInstructionLength(INSTRUCTION_TABLE, inst);
		if (!l)
			return ret;

		inst += l;
		length += l;
	}

	BYTE jmp[] = { 0xFF, 0x25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, };
	PVOID copy = ExAllocatePool(NonPagedPool, length + sizeof(jmp));
	if (copy)
	{
		memcpy(copy, src, length);
		*(PVOID*)&jmp[6] = (PBYTE)src + length;
		memcpy((PBYTE)copy + length, jmp, sizeof(jmp));

		BYTE hook[] = { 0xFF, 0x25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90 };
		*(PVOID*)&hook[6] = dest;

		PMDL mdl = IoAllocateMdl(src, length, 0, 0, 0);
		if (mdl)
		{
			MmProbeAndLockPages(mdl, KernelMode, IoModifyAccess);

			PVOID mapped = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmNonCached, 0, 0, HighPagePriority);
			if (mapped)
			{
				memcpy(mapped, hook, length);
				MmUnmapLockedPages(mapped, mdl);

				ret = TRUE;
			}
			else
			{
				DbgPrintEx(0, 0, "[!] Failed to map pages\n");
			}

			MmUnlockPages(mdl);
			IoFreeMdl(mdl);
		}
		else
		{
			DbgPrintEx(0, 0, "[!] Failed to allocate MDL\n");
		}

		if (ret)
		{
			*original = copy;
			return ret;
		}
		else
		{
			ExFreePool(copy);
		}
	}
	else
	{
		DbgPrintEx(0, 0, "[!] Failed to allocate gate\n");
	}

	return ret;
}

BOOL UnTrampolineHook(PVOID src, PVOID original)
{
	BOOL ret = FALSE;

	BYTE length = 0;
	for (PBYTE inst = (PBYTE)original; length < JMP_SIZE; )
	{
		BYTE l = GetInstructionLength(INSTRUCTION_TABLE, inst);
		if (!l)
			return ret;

		inst += l;
		length += l;
	}

	PMDL mdl = IoAllocateMdl(src, length, 0, 0, 0);
	if (mdl)
	{
		MmProbeAndLockPages(mdl, KernelMode, IoModifyAccess);

		PVOID mapped = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmNonCached, 0, 0, HighPagePriority);
		if (mapped)
		{
			memcpy(mapped, original, length);
			MmUnmapLockedPages(mapped, mdl);
			ExFreePool(original);

			ret = TRUE;
		}
		else
		{
			DbgPrintEx(0, 0, "[!] Failed to map pages\n");
		}

		MmUnlockPages(mdl);
		IoFreeMdl(mdl);
	}
	else
	{
		DbgPrintEx(0, 0, "[!] Failed to allocate MDL\n");
	}

	return ret;
}